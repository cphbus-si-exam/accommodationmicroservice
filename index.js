const amqp = require('amqplib');
const fetch = require('node-fetch');

const config = require('./config');

console.log("acc service is starting");

(async() => {

    const connection = await amqp.connect(config.rabbitmq);
    const channel = await connection.createChannel();

    channel.assertExchange("microservice_ex",  "direct", {durable: false});
    channel.assertExchange("msres_ex", "direct", {durable: false});

    let queue = await channel.assertQueue("", {exclusive: true});
    channel.bindQueue(queue.queue, "microservice_ex", "Accommodation")
    
    console.log("accommodation service is running, queue name", queue.queue)

    channel.consume(queue.queue,async msg => {

        let message = JSON.parse(msg.content.toString());
        console.log(message);

        let accRes = await (await fetch(config.accummadation_endpoint+'/accommodation', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(message)
        })).json() 

        accRes.type = "accomodation"

        console.log(accRes)

        console.log(channel.publish("msres_ex","",Buffer.from(JSON.stringify(accRes))))
    }, {noAck: true})

})()